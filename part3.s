.include	"address_map_arm.s"
				.include	"interrupt_ID.s"
				.include "config_GIC2.s"
				globalVar: .word 0 // global variable 
				CHAR_BUFFER:	.word 		0 // CHAR_BUFFER for part 3 of the lab
				CHAR_FLAG:		.word 		0 // CHAR_FLAG for part 3 of the lab

	///////	/* ********************************************************************************
// * This program demonstrates use of interrupts with assembly language code. 
/// * The program responds to interrupts from the pushbutton KEY port in the FPGA.
/// *
/// * The interrupt service routine for the pushbutton KEYs indicates which KEY has 
/// * been pressed on the HEX0 display.
/// ********************************************************************************/

				.section .vectors, "ax"

				B 			_start					// reset vector
				B 			SERVICE_UND				// undefined instruction vector
				B 			SERVICE_SVC				// software interrrupt vector
				B 			SERVICE_ABT_INST		// aborted prefetch vector
				B 			SERVICE_ABT_DATA		// aborted data vector
				.word 	0							// unused vector
				B 			SERVICE_IRQ				// IRQ interrupt vector
				B 			SERVICE_FIQ				// FIQ interrupt vector
				

				.text
				.globl	_start
_start:		
				///* Set up stack pointers for IRQ and SVC processor modes */
				MOV		R1, #0b11010010					// interrupts masked, MODE = IRQ
				MSR		CPSR_c, R1							// change to IRQ mode
				LDR		SP, =A9_ONCHIP_END - 3			// set IRQ stack to top of A9 onchip memory
				//* Change to SVC (supervisor) mode with interrupts disabled */
				MOV		R1, #0b11010011					// interrupts masked, MODE = SVC
				MSR		CPSR, R1								// change to supervisor mode
				LDR		SP, =DDR_END - 3					// set SVC stack to top of DDR3 memory

				BL			CONFIG_GIC							// configure the ARM generic interrupt controller

				// write to the pushbutton KEY interrupt mask register
				LDR		R0, =KEY_BASE						// pushbutton KEY base address
				MOV		R1, #0xF								// set interrupt mask bits
				STR		R1, [R0, #0x8]						// interrupt mask register is (base + 8)

				// enable IRQ interrupts in the processor
				MOV		R0, #0b01010011					// IRQ unmasked, MODE = SVC
				MSR		CPSR_c, R0

	///*   configure timer    */

				///CONFIGURE THE TIMER


LDR R1, =MPCORE_PRIV_TIMER // this register contains the MPCore private timer base address
LDR R4, =100000000 // time = 1/(200 MHz) x 100×10∧6 = 0.5 sec
STR R4, [R1] // write to timer load register ( storing and putting that load value  in another register)
MOV R4, #0b111 // set bits: auto = 1 (Auto (A)), enable = 1 (Enable (E)), interrupt =1 (Interrupt (I))   /// this part allows us to change individual bits of the register
STR R4, [R1, #0x8] // write to timer control register to set those registers

// CONFIGURE THE KEYBOARD INTERRUPT

LDR R1, =JTAG_UART_BASE  // this register contains the base address of the keyboard interrupt
MOV R3, #0b01 			// we move the RE bit to 1 and set WE bit to 0
STR R3, [R1, #0x4]  		// in this step we write the RE bit to 1


IDLE:
/*Modify the IDLE loop to read the global variable
 “CHAR_FLAG”. If the value of “CHAR_FLAG” is 1
then read the value of “CHAR_BUFFER” into register 
R0 then call PUT_JTAG and set “CHAR_FLAG” to
0. */

LDR R1, =CHAR_FLAG
LDR R0, [R1]
CMP R0, #1

/*If the value of “CHAR_FLAG” is 1
then read the value of “CHAR_BUFFER” into register 
R0*/
BEQ SET_CHAR_BUFFER


		B 			IDLE									// main program simply idles

SET_CHAR_BUFFER:
LDR R0, =CHAR_BUFFER//read the value of “CHAR_BUFFER” into register R0
BL PUT_JTAG // Branch and link to PUT_JTAG

MOV R1, #0 // value to set char_flag to 0
LDR R0, =CHAR_FLAG //read CHAR_FLAG into register R0

STR R1, [R0] // write to R0 set the CHAR_FLAG to 0
B IDLE // no where else to go so set back to IDLE loop



PUT_JTAG: LDR R1, =0xFF201000 // JTAG UART base address
LDR R2, [R1, #4] // read the JTAG UART control register
LDR R3, =0xFFFF
ANDS R2, R2, R3 // check for write space
BEQ END_PUT // if no space, ignore the character
STR R0, [R1] // send the character
END_PUT: BX LR

////* Define the exception service routines */

///*--- Undefined instructions --------------------------------------------------*/
SERVICE_UND:
    			B SERVICE_UND 
 
////*--- Software interrupts -----------------------------------------------------*/
SERVICE_SVC:			
    			B SERVICE_SVC 

///*--- Aborted data reads ------------------------------------------------------*/
SERVICE_ABT_DATA:
    			B SERVICE_ABT_DATA 

////*--- Aborted instruction fetch -----------------------------------------------*/
SERVICE_ABT_INST:
    			B SERVICE_ABT_INST 
 
////*--- IRQ ---------------------------------------------------------------------*/
SERVICE_IRQ:
    			PUSH		{R0-R7, LR}
    
    			/* Read the ICCIAR from the CPU interface */
    			LDR		R4, =MPCORE_GIC_CPUIF
    			LDR		R5, [R4, #ICCIAR]				// read from ICCIAR
// apparently this returns the interrupt ID to determine which interrupt it was
// very important b/c we used a bit in a register to determine which of the interrupts it was which was a wrong approach
				// FOR THE TIMER INTERRUPT
				CMP R5, #29 // ID of the MP private timer interrupt
				BEQ TIMER_INTERRUPT
				
				// FOR THE KEYBOARD INTERRUPT
				CMP R5, #80 // ID of the JTAG IRQ interrupt
				BEQ KEYBOARD_INTERRUPT
				
				
				
FPGA_IRQ1_HANDLER:
    			CMP		R5, #KEYS_IRQ
UNEXPECTED:	BNE		UNEXPECTED    					// if not recognized, stop here
    
    			BL			KEY_ISR
EXIT_IRQ:
    			/* Write to the End of Interrupt Register (ICCEOIR) */
    			STR		R5, [R4, #ICCEOIR]			// write to ICCEOIR
    
    			POP		{R0-R7, LR}
    			SUBS		PC, LR, #4

TIMER_INTERRUPT: 
LDR	R3, =LEDR_BASE		// Register R3 will contain the base address of the LEDS	
LDR R1, [R3]	// Read the LEDR status output
ADD R1, R1, #1			// increment global variable by 1	
STR R1, [R3] 		// 	write the number to memory and LED	
// originally had a loop but got stuck in an infinite loop until we got help from a TA
B EXIT_IRQ // since we dont want it to go to SERVICE_FIQ we set an unconditional branch back to EXIT_IRQ
				
				
KEYBOARD_INTERRUPT:
MOV R3, #1 // will be used later to set the CHAR_FLAG to 1 (R3 =1)
MOV R0, #0b11111111 // this number will be anded with data register to see the representation and to get memory
// use scrap registers instead of r4-r10 b/c dont have to worry about stack pointers
LDR R1, =JTAG_UART_BASE // R1 will contain the base address JTAG_UART_BASE
LDR R2, [R1] // Read the data register from memory to read from keyboard
AND R2, R2, R0 // stores the data we and into R2
LDR R1, =CHAR_BUFFER // reads CHAR_BUFFER from memory into R1
STR R2, [R1] // writes the 8-bit ASCII encoded character into CHAR_BUFFER 
LDR R1, =CHAR_FLAG // reads the value of CHAR_FLAG from memory into R1
STR R3, [R1] // stores 1 inside the CHAR_FLAG 

B EXIT_IRQ

				
/////*--- FIQ ---------------------------------------------------------------------*/
SERVICE_FIQ:
    			B			SERVICE_FIQ 

				.end   
				
				